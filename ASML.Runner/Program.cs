﻿using System;
using ASML.Lib;

namespace ASML.Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            DisplayOptions();
            int option = Convert.ToInt16(Console.ReadLine());

            switch (option)
            {
                case 1:
                    {
                        var result = SumOfMultiple.SumOfAllNaturalNumbers();
                        Console.WriteLine($"The result of SumOfAllNaturalNumbers is {result}");
                    }
                    break;
                case 2:
                    {
                        var result = SequenceAnalysis.FinalAllUppercaseWordsAlphabetically();
                        Console.WriteLine($"The result of FinalAllUppercaseWordsAlphabetically is {result}");
                    }
                    break;
                case 3:
                    {
                        var result = Divisibility.Find();
                        Console.WriteLine($"The result of IsXDivisibleByY is {result}");
                    }
                    break;
                default:
                    Console.WriteLine("Invalid options");
                    break;
            }
        }


        static void DisplayOptions()
        {
            Console.WriteLine("1. Press 1 to execute SumOfMultiple");
            Console.WriteLine("2. Press 2 to execute SequenceAnalysis");
            Console.WriteLine("3. Press 3 to execute IsXDivisibleByY");
        }
    }
}

