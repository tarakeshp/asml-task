﻿using System;
namespace ASML.Lib
{
    public sealed class Divisibility
    {
        public static int Find()
        {
            Console.Write("Enter X Value:");
            var xValue = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter Y Value:");
            var yValue = Convert.ToInt32(Console.ReadLine());

            var modValue = xValue % yValue;
            var negateBlnValue = !Convert.ToBoolean(modValue);
            Console.WriteLine($"Mod:{modValue} ~Boolean:{negateBlnValue}");

            return Convert.ToInt16(negateBlnValue);
        }
    }
}
