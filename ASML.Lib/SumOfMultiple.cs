﻿using System;

namespace ASML.Lib
{
    public sealed class SumOfMultiple
    {
        public static int SumOfAllNaturalNumbers()
        {
            int sum = 0;

            Console.WriteLine("Enter multiple number. For eg: 3");
            var mutipleValue = Convert.ToInt16(Console.ReadLine());

            Console.WriteLine("Enter limit number. For eg: 100");
            var limitValue = Convert.ToInt16(Console.ReadLine());

            if (mutipleValue <= 1) throw new Exception("Multiple number should be greater than 1");
            if (limitValue <= 2) throw new Exception("Multiple number should be greater than 2");

            for (int i = 1; i < limitValue + 1; i++)
            {
                if (i % mutipleValue == 0)
                {
                    sum += i;
                    Console.WriteLine($"OK: Natural Number:{i}, Sum:{sum}");
                }
            }

            return sum;
        }
    }
}
