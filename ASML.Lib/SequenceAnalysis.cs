﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace ASML.Lib
{
    public sealed class SequenceAnalysis
    {
        public static string FinalAllUppercaseWordsAlphabetically()
        {            
            Console.WriteLine("Enter any string. For eg: This IS a STRING and press Enter");
            var input = Console.ReadLine();

            if (input.Length <= 0) throw new Exception("Input should not empty");

            List<int> asciiList = new List<int>();
            foreach(char c in input)
            {
                int charValue = (int)c;
                if (charValue >= 65 && charValue <= 90)
                {
                    asciiList.Add(charValue);
                }
            }
            asciiList.Sort();
            return String.Join(String.Empty, asciiList.Select(x => ((char)x).ToString()));
        }
    }
}
